//
//  FQTip.swift
//  FQDemo
//
//  Created by TriVM on 1/20/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import Foundation
import SwiftyJSON

class FQTip: NSObject{
    var canonicalUrl: String?
    //var photourl: String?
    var firstName: String?
    var lastName: String?
    var text: String?
    var prefix: String?
    var suffix: String?
    
    init(_ json:JSON) {
        self.canonicalUrl = json["canonicalUrl"].stringValue
        //self.photourl = json["photourl"].stringValue
        self.firstName = json["user"]["firstName"].stringValue
        self.lastName = json["user"]["lastName"].stringValue
        self.text = json["text"].stringValue
        self.prefix = json["user"]["photo"]["prefix"].stringValue
        self.suffix = json["user"]["photo"]["suffix"].stringValue
    }
    
    static func toArrayTip(_ items:[JSON]) -> [FQTip]{
        var arrTip = [FQTip]()
        for  i in 0..<items.count {
            let itemJSON = items[i]
            let tip = FQTip(itemJSON)
            arrTip.append(tip)
        }
        return arrTip
    }
}
