//
//  FQVenue.swift
//  FQDemo
//
//  Created by TriVM on 1/18/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import Foundation
import SwiftyJSON
import MapKit
import Contacts

class FQVenue: NSObject, MKAnnotation {
    
    var name:String?
    var id:String?
    var rating:Double?
    var distance:Int?
    var address:String?
    var phone:String?
    var latitude: Double?
    var longitude:Double?
    var people:String?
    var categoriyShortName:String?
    var coordinate: CLLocationCoordinate2D
    var tips = [FQTip]()
    var photos = [String]()
    var category : FQCategory?
    var title: String?
    var subtitle: String?
    var isMyLocation : Bool?
    var tintColor: UIColor?
    
    
    init(_ json:JSON, recommend:Bool){
        
        var jsonTemp = json
        if recommend {
            jsonTemp = json["venue"]
            if(jsonTemp["categories"].arrayValue.count > 0){
                self.categoriyShortName = jsonTemp["categories"][0]["shortName"].stringValue
            }
        }
        self.name = jsonTemp["name"].stringValue
        self.id = jsonTemp["id"].stringValue
        self.rating = jsonTemp["rating"].doubleValue
        self.distance = jsonTemp["location"]["distance"].intValue
        self.address = jsonTemp["location"]["address"].stringValue
        self.latitude = jsonTemp["location"]["lat"].doubleValue
        self.longitude = jsonTemp["location"]["lng"].doubleValue
        self.people = jsonTemp["hereNow"]["summary"].stringValue
        self.phone = jsonTemp["contact"]["phone"].stringValue
        self.coordinate = CLLocationCoordinate2D(latitude: self.latitude!, longitude: self.longitude!)
        
        self.title = self.name
        self.tintColor = UIColor.blue
        self.subtitle = String(format:"Address: %@\nPhone: %@\nRating: %0.1f\n%@", self.address!, self.phone!, self.rating!, self.people!)
        
        if (jsonTemp["categories"].arrayValue.count > 0) {
            self.category = FQCategory(jsonTemp["categories"][0])
        }
    }
    
    func parsePhotoURL(_ json: JSON) -> [String]{
        var photos = [String]()
        let items = json["response"]["photos"]["items"].arrayValue
        for i in 0..<items.count{
            let itemJSON = items[i]
            let height = itemJSON["height"].intValue
            let width = itemJSON["width"].intValue
            let prefix = itemJSON["prefix"].stringValue
            let suffix = itemJSON["suffix"].stringValue
            let sizeStr = String(format: "%dx%d", width, height)
            let url = String(format: "%@%@%@", prefix, sizeStr, suffix)
            
            photos.append(url)
        }
        return photos
    }
    
    static func toArrayVenue(_ json:JSON) -> [FQVenue]{
        var arrVenue = [FQVenue]()
        //let group = json["response"]["groups"][0]
        let items = json["response"]["venues"].arrayValue
        for  i in 0..<items.count {
            let itemJSON = items[i]
            let venue = FQVenue(itemJSON, recommend: false)
            arrVenue.append(venue)
        }
        return arrVenue
    }
    
    static func toArrayVenueCommand(_ json:JSON) -> [FQVenue]{
        var arrVenue = [FQVenue]()
        let group = json["response"]["groups"][0]
        let items = group["items"].arrayValue
        for  i in 0..<items.count {
            let itemJSON = items[i]
            let venue = FQVenue(itemJSON, recommend: true)
            arrVenue.append(venue)
        }
        return arrVenue
    }
    
    static func toArrayVenueSuggest(_ json:JSON) -> [FQVenue]{
        var arrVenue = [FQVenue]()
        let items = json["response"]["minivenues"].arrayValue
        for  i in 0..<items.count {
            let itemJSON = items[i]
            let venue = FQVenue(itemJSON, recommend: false)
            arrVenue.append(venue)
        }
        return arrVenue
    }
    
    func getDetail(completion: @escaping (_ data: JSON?, _ error: Error?) -> Void) {
        CommonService.venueDetail(self, numberItem: 50) { (json, error) in
            completion(json, error)
        }
    }
    
    func getTips(completion: @escaping (_ successful: Bool, _ error: Error?) -> Void ){
        CommonService.venueTips(self, numberItem: 50) { (json, error) in
            if(error == nil){
                self.tips = FQTip.toArrayTip(json!["response"]["tips"]["items"].arrayValue)
                completion(true, error)
            }else{
                completion(false,  error)
            }
        }
    }
    
    func getPhotos(completion: @escaping (_ successful: Bool, _ error: Error?) -> Void ){
        CommonService.venuePhotos(self, numberItem: 12) { (json, error) in
            if(error == nil){
                self.photos = self.parsePhotoURL(json!)
                completion(true, error)
            }else{
                completion(false, error)
            }
        }
    }
}

