//
//  FQCategory.swift
//  FQDemo
//
//  Created by TriVM on 1/23/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import UIKit
import SwiftyJSON

class FQCategory: NSObject {
    
    var name: String?
    var prefix: String?
    var suffix: String?
    
    init(_ json:JSON) {
        self.name = json["shortName"].stringValue
        self.prefix = json["icon"]["prefix"].stringValue
        self.suffix = json["icon"]["suffix"].stringValue
    }
    
    static func toArrayCategory(_ json:JSON) -> [FQCategory]{
        var arrCategory =  [FQCategory]()
        let items = json["response"]["categories"].arrayValue
        for  i in 0..<items.count {
            let itemJSON = items[i]
            let category = FQCategory(itemJSON)
            arrCategory.append(category)
            let arrCategoryTemp = FQCategory.toChilArrayCategory(itemJSON)
            arrCategory += arrCategoryTemp
        }
        
        return arrCategory
    }
    
    static func toChilArrayCategory(_ json:JSON) -> [FQCategory]{
        var arrCategory =  [FQCategory]()
        let items = json["categories"].arrayValue
        for  i in 0..<items.count {
            let itemJSON = items[i]
            let category = FQCategory(itemJSON)
            arrCategory.append(category)
            
            let arrCategoryTemp = FQCategory.toChilArrayCategory(itemJSON)
            arrCategory += arrCategoryTemp
        }
        return arrCategory
    }

}
