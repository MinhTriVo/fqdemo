//
//  FQTipsVC.swift
//  FQDemo
//
//  Created by TriVM on 1/21/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import UIKit
import Auk
import moa

class FQTipsVC: UIViewController {
    
    //MARK: - OUTLET
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var mSlideImageScrollView: UIScrollView!
    
    //MARK: - PROPERTY
    var currentVenue: FQVenue!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupView()
        getPhotos()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        mSlideImageScrollView.auk.stopAutoScroll()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Setup
    
    func setupView() {
        self.title = self.currentVenue.name!
        self.mTableView.tableFooterView = UIView()
        self.mTableView.reloadData()
        self.mTableView.allowsSelection = false
        setupScrollView()
    }
    
    func setupScrollView(){
        mSlideImageScrollView.delegate = self
        mSlideImageScrollView.auk.settings.placeholderImage = UIImage(named: "no-image-icon")
        mSlideImageScrollView.auk.settings.errorImage = UIImage(named: "no-image-icon")
        
        // Turn on the image logger. The download log will be visible in the Xcode console
        //Moa.logger = MoaConsoleLogger
        
        mSlideImageScrollView.auk.startAutoScroll(delaySeconds: 2)
    }
    
    //MARK: - Methods
    func getPhotos(){
        FQUtilities.showLoading(FQString.loading)
        currentVenue.getPhotos { (successful, error) in
            FQUtilities.hideLoading()
            if(successful){
                if(self.currentVenue.photos.count > 0){
                    for url in self.currentVenue.photos {
                        self.mSlideImageScrollView.auk.show(url: url)
                    }
                }
                else{
                    if let image = UIImage(named: "no-image-icon") {
                        self.mSlideImageScrollView.auk.show(image: image)
                    }
                }
                
            }else{
                FQUtilities.showAlert(self, title: FQString.Alert.error, message: FQString.Alert.messagePhotosFail)
            }
        }
    }
}

extension FQTipsVC: UITableViewDelegate, UITableViewDataSource{
    // MARK: - TableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.currentVenue.tips.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FQTipTableViewCell") as! TipCell
        
        let tip = self.currentVenue.tips[indexPath.row]
        
        cell.setupView(tip)
       
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120;
    }
}
