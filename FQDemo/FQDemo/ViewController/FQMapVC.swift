//
//  FQMapVC.swift
//  FQDemo
//
//  Created by TriVM on 1/19/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import SwiftyJSON
import DropDown
import CoreLocation

class FQMapVC: UIViewController {
    
    //MARK: - OUTLET
    @IBOutlet weak var mMapView: MKMapView!
    @IBOutlet weak var mRightBarButton: UIBarButtonItem!
    //MARK: - PROPERTY
    let locationManager = CLLocationManager()
    let regionRadius: CLLocationDistance = 500
    var currentLocation:CLLocationCoordinate2D?
    var currentVenue: FQVenue!
    var centerMap: FQVenue!
    var venueSelected: FQVenue!
    var commandResults = [FQVenue]()
    var categories = [FQCategory]()
    let rightBarDropDown = DropDown()
    var categorySelected: String?
    var countdownTimer = Timer()
    
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        //getCategories()
        getRecommandCategories(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "MapPushToVenueTips") {
            let desVC = segue.destination as! FQTipsVC
            let venue = sender as! FQVenue
            desVC.currentVenue = venue
        }
    }
    
    //MARK: - setup
    func setupView(){
        self.navigationController?.navigationBar.isHidden = false
        categorySelected = ""
        centerMap = currentVenue
        currentLocation = CLLocationCoordinate2D(latitude: centerMap.coordinate.latitude, longitude: centerMap.coordinate.longitude)
        centerMapOnLocation(location: centerMap)
        self.mMapView.delegate = self
        setupCurrentAnnotation()
    }
    
    func setupCurrentAnnotation(){
        self.mMapView.addAnnotation(centerMap)
        self.mMapView.selectAnnotation(centerMap, animated: true)
    }
    
    func setupRightbarButton(_ data:[String]){
        rightBarDropDown.anchorView = mRightBarButton
        rightBarDropDown.direction = .bottom
        
        var dataSource = data
//        for i in 0..<categories.count{
//            dataSource.append(categories[i].name!)
//        }

        
        dataSource = Array(Set(dataSource))
        
        DropDown.appearance().cellHeight = 40
        rightBarDropDown.dataSource = dataSource
        rightBarDropDown.width = 200;
   
        
        rightBarDropDown.selectionAction = { [weak self] (index, item) in
            self?.categorySelected = dataSource[index]
            self?.title = self?.categorySelected
            self?.getRecommand((self?.categorySelected)!, isShowLoading: true)
        }
        
    }
    
    //MARK: - actions
    @IBAction func showBarButtonDropDown(_ sender: AnyObject) {
        rightBarDropDown.show()
    }
    
    @IBAction func didMoveCurrentLoc(_ sender: Any) {
        if CLLocationManager.locationServicesEnabled() {
            let region = MKCoordinateRegionMakeWithDistance(currentLocation!, regionRadius, regionRadius)
            self.mMapView.setRegion(region, animated: true)
            locationManager.startUpdatingLocation()
        }
    }
    
    // MARK: - methods
    
    func checkVenueInList(_ venue:FQVenue, list:[FQVenue]) -> Bool {
        for i in 0..<list.count {
            let venueTemp = list[i]
            if (venue.id == venueTemp.id){
                return true
            }
        }
        
        return false
    }
    
    func centerMapOnLocation(location: FQVenue) {
        let currentLocation = CLLocationCoordinate2D(latitude: location.latitude!, longitude: location.longitude!)
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(currentLocation, regionRadius, regionRadius)
        mMapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addNewAnnotation(){
        
        let annotationsMap = Array( self.mMapView.annotations)
        let annotationsCommands = Array(self.commandResults)
        
        for i in 0..<annotationsMap.count{
            let annation = annotationsMap[i] as! FQVenue
            if(!checkVenueInList(annation, list: annotationsCommands)){
                self.mMapView.removeAnnotation(annation)
            }
        }
        
        for i in 0..<annotationsCommands.count{
            let annation = annotationsCommands[i]
            if(!checkVenueInList(annation, list: annotationsMap as! [FQVenue])){
                self.mMapView.addAnnotation(annation)
            }
        }
    }
    
    func checkVenueInMap(_ venue:FQVenue, annotations:[FQVenue]) -> Bool{
        for i in 0..<annotations.count{
            let annation = annotations[i]
            if(annation.coordinate.latitude == venue.coordinate.latitude &&
                annation.coordinate.longitude == venue.coordinate.longitude){
                return true
            }
        }
        return false
    }
    
    func getRecommand(_ category:String, isShowLoading:Bool){
        if(isShowLoading){
            FQUtilities.showLoading(FQString.loading)
        }
        CommonService.venueRecommend(currentVenue.coordinate, category: category, numberItem: 50) { (json, error) in
            FQUtilities.hideLoading()
            if(error == nil){
                //self.mMapView.removeAnnotations(self.commandResults)
                
                self.commandResults.removeAll()
                self.commandResults = FQVenue.toArrayVenueCommand(json!)
                
                //self.mMapView.addAnnotations(self.commandResults)
                self.addNewAnnotation()
                //print("commandResults.count = \(self.commandResults.count)\n map = \(self.mMapView.annotations.count)")
            }else{
                FQUtilities.showAlert(self, title: FQString.Alert.error, message: (error?.localizedDescription)!)
            }
        }
    }
    
    func getRecommandCategories(_ isShowLoading:Bool){
        if(isShowLoading){
            FQUtilities.showLoading(FQString.loading)
        }
        CommonService.venueRecommend(currentVenue.coordinate, category: "", numberItem: 50) { (json, error) in
            FQUtilities.hideLoading()
            if(error == nil){
                let results = FQVenue.toArrayVenueCommand(json!)
                var dataSource = [String]()
                for i in 0..<results.count{
                    dataSource.append(results[i].categoriyShortName!)
                }
                self.setupRightbarButton(dataSource)
            }else{
                FQUtilities.showAlert(self, title: FQString.Alert.error, message: (error?.localizedDescription)!)
            }
        }
    }
    
    /*func getCategories(){
        FQUtilities.showLoading(userInteractionStatus: false)
        CommonService.venueCategories(currentVenue.coordinate){ (json, error) in
            FQUtilities.hideLoading()
            if(error == nil){
                self.categories = FQCategory.toArrayCategory(json!)
                if(self.categories.count > 0){
                    var dataSource = [String]()
                    for i in 0..<self.categories.count{
                        dataSource.append(self.categories[i].name!)
                    }
                    self.setupRightbarButton(dataSource)
                }
            }else{
                FQUtilities.showAlert(self, title: FQString.Alert.error, message: (error?.localizedDescription)!)
            }
        }
    }*/
    
    func checkCenterMap(){
        startTimer()
    }
    
    func checkVisibleCurrentLoc(){
        if #available(iOS 9.0, *) {
        } else {
            let coordinateOld = CLLocation(latitude: currentLocation!.latitude, longitude: currentLocation!.longitude)
            let coordinateNew = CLLocation(latitude: mMapView.centerCoordinate.latitude, longitude: mMapView.centerCoordinate.longitude)
            
            let distanceInMeters = coordinateOld.distance(from: coordinateNew)
            if (distanceInMeters > regionRadius) {
                mMapView.removeAnnotation(centerMap)
            }else{
                mMapView.addAnnotation(centerMap)
            }
        }
    }
    
    //MARK: - Timer
    func startTimer() {
        endTimer()
        countdownTimer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        
    }
    
    @objc func updateTime() {
        endTimer()
        let coordinateOld = CLLocation(latitude: currentVenue.coordinate.latitude, longitude: currentVenue.coordinate.longitude)
        let coordinateNew = CLLocation(latitude: mMapView.centerCoordinate.latitude, longitude: mMapView.centerCoordinate.longitude)
        
        let distanceInMeters = coordinateOld.distance(from: coordinateNew)
        checkVisibleCurrentLoc()
        if (distanceInMeters > ((regionRadius*80)/100)) {
            //print(distanceInMeters)
            currentVenue.coordinate = mMapView.centerCoordinate
            getRecommandCategories(false)
            if(!categorySelected!.isEmpty){
                getRecommand(categorySelected!, isShowLoading: false)
            }
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
    }
}

// MARK: - MKMapViewDelegate

extension FQMapVC: MKMapViewDelegate {
 
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
       
        guard let annotation = annotation as? FQVenue else { return nil }
        let identifier = "AnnotationViewIdentifier"
        var view:MKAnnotationView?
        
        if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) {
            annotationView.annotation = annotation
            view = annotationView
        } else {
            let annotationView = MKPinAnnotationView(annotation:annotation, reuseIdentifier:identifier)
            setupAnnotionView(annotationView, annotation: annotation)
            view = annotationView
        }
        
        setDataAnnotionView(view as! MKPinAnnotationView, annotation: annotation)
        return view
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        venueSelected = view.annotation as! FQVenue
        
    }
    
    func setupAnnotionView(_ annotationView: MKPinAnnotationView, annotation: FQVenue) {
        annotationView.isEnabled = true
        annotationView.canShowCallout = true
        //annotationView.pinTintColor = annotation.tintColor
        if #available(iOS 9, *) {
            annotationView.pinTintColor = annotation.tintColor
        } else {
            annotationView.pinColor = .green
        }
        
        let detailLabel = UILabel()
        detailLabel.numberOfLines = 0
        detailLabel.font = detailLabel.font.withSize(12)
        if #available(iOS 9.0, *) {
            annotationView.detailCalloutAccessoryView = detailLabel
        } else {
            annotationView.leftCalloutAccessoryView = detailLabel
        }
        
        let detailButton: UIButton = UIButton(type: UIButtonType.detailDisclosure)
        detailButton.addTarget(self, action: #selector(didDetailAnnotationButton(sender:)), for: .touchUpInside)
        annotationView.rightCalloutAccessoryView = detailButton
    }
    
    func setDataAnnotionView(_ annotationView: MKPinAnnotationView, annotation: FQVenue) {
        var detailLabel: UILabel!
        if #available(iOS 9.0, *) {
            detailLabel = annotationView.detailCalloutAccessoryView as! UILabel
        } else {
            detailLabel = annotationView.leftCalloutAccessoryView as! UILabel
        }
        detailLabel.text = annotation.subtitle
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool){
        checkCenterMap()
    }
    
    @objc func didDetailAnnotationButton(sender: AnyObject) {
        FQUtilities.showLoading(FQString.loading)
        venueSelected.getTips { (successful, error) in
            FQUtilities.hideLoading()
            if(successful){
                if(self.venueSelected.tips.count > 0){
                    self.performSegue(withIdentifier: "MapPushToVenueTips", sender: self.venueSelected)
                }
                else{
                    FQUtilities.showAlert(self, title: FQString.Alert.waring, message: FQString.Alert.messageNoTips)
                }
            }else{
                 FQUtilities.showAlert(self, title: FQString.Alert.error, message: FQString.Alert.messageTipsFail)
            }
        }
    }
}
