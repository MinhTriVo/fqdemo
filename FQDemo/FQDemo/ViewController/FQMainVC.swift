//
//  ViewController.swift
//  FQDemo
//
//  Created by TriVM on 1/18/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import UIKit
import CoreLocation
import FoursquareAPIClient
import SwiftyJSON
import Alamofire

class FQMainVC: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    //MARK: - OUTLET
    @IBOutlet weak var mSearchTextField: UITextField!
    @IBOutlet weak var mTableView: UITableView!
    
    //MARK: - PROPERTY
    var currentLocation:CLLocationCoordinate2D?
    let locationManager = CLLocationManager()
    var searchResults = [FQVenue]()
    var suggestResults = [FQVenue]()
    var heightHeaderTableView:Int = 50;
    var titleHeaderTableView:String!;
    let reachabilityManager = NetworkReachabilityManager()
    
    var countdownTimer = Timer()

    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        setupLocationManager()
        getCurrentLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "SegueMainPushToMap") {
            let desVC = segue.destination as! FQMapVC
            let venue = sender as! FQVenue
            desVC.currentVenue = venue
        }
    }
    
    // MARK: - Setup
    
    func setupView() {
        self.mTableView.tableFooterView = UIView()
        self.mSearchTextField.clearButtonMode = .whileEditing
        self.mSearchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        titleHeaderTableView = FQString.no_data
        setupTitleHeaderTableView()
        
        reachabilityManager?.startListening()
        reachabilityManager?.listener = { _ in
            self.setupTitleHeaderTableView()
        }
    }
    
    func setupTitleHeaderTableView(){
        if let isNetworkReachable = self.reachabilityManager?.isReachable,
            isNetworkReachable == true {
            //Internet Available
            self.titleHeaderTableView = FQString.no_data
            if(self.searchResults.count == 0){
                self.heightHeaderTableView = 50
            }else {
                self.heightHeaderTableView = 0
            }
            self.mTableView.reloadData()
        } else {
            //Internet Not Available"
            self.heightHeaderTableView = 50
            self.titleHeaderTableView = FQString.no_internet
            self.mTableView.reloadData()
        }
    }
    
    
    func setupLocationManager() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    }
    
    // MARK: - Methods
    func getCurrentLocation() {
        FQUtilities.showLoading(FQString.getCurrentLocation)
        // check if access is granted
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .authorizedWhenInUse:
                locationManager.startUpdatingLocation()
            case .authorizedAlways:
                locationManager.startUpdatingLocation()
            case .notDetermined, .restricted, .denied:
                FQUtilities.hideLoading()
                FQUtilities.showAlert(self, title: FQString.Alert.waring, message: FQString.Alert.messageEnanleLocation)
            }
        } else {
            FQUtilities.hideLoading()
            let alert = UIAlertController(title: FQString.Alert.waring, message: FQString.Alert.messageWarningEnanleLocation, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: FQString.Alert.dismissButton, style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: FQString.Alert.okButton, style: UIAlertActionStyle.default, handler: {_ in
                if #available(iOS 10.0, *)
                {
                    UIApplication.shared.openURL(URL(string: "App-Prefs:root=LOCATION_SERVICES")!)
                }
                else
                {
                    UIApplication.shared.openURL(URL(string: "prefs:root=LOCATION_SERVICES")!)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func checkCurrentLocation() -> Bool {
        
        if currentLocation == nil {
            return false
        }
        return true
    }
    
    func checkKeySearch() -> Bool {
        
        let keySearch = self.mSearchTextField.text!
        if (keySearch.isEmpty) {
            FQUtilities.showAlert(self, title: FQString.Alert.waring , message: FQString.Alert.messageSearch)
            return false;
        }
        return true
    }
    
    func didSearch(){
        self.view.endEditing(true)
        
        if !checkCurrentLocation() {
            FQUtilities.showAlert(self, title: FQString.Alert.waring, message: FQString.Alert.messageEnanleLocation)
            return
        }
        if !checkKeySearch() {
            return
        }
        
        self.searchResults.removeAll()
        FQUtilities.showLoading(FQString.loading)
        
        CommonService.venueSearch(currentLocation!, keySearch: self.mSearchTextField.text!, numberItem: 50) { (json, error) in
            FQUtilities.hideLoading()
            if(error == nil){
                self.searchResults = FQVenue.toArrayVenue(json!)
                if(self.searchResults.count == 0){
                    self.heightHeaderTableView = 50
                }else {
                    self.heightHeaderTableView = 0
                }
                
                self.mTableView.reloadData()
            }else{
                FQUtilities.showAlert(self, title: FQString.Alert.error, message: (error?.localizedDescription)!)
            }
        }
    }
    
    func getVeneSuggest(_ searchText: String) {
        
        if (!checkCurrentLocation()){
            FQUtilities.showAlert(self, title: FQString.Alert.waring, message: FQString.Alert.getCurrentLocationError)
            return;
        }
        
        FQUtilities.showLoading(FQString.loading)
        CommonService.venueSuggest(currentLocation!, keySearch: searchText, numberItem: 50) { (json, error) in
            FQUtilities.hideLoading()
            if(error == nil){
                self.searchResults = FQVenue.toArrayVenueSuggest(json!)
                if(self.searchResults.count == 0){
                    self.heightHeaderTableView = 50
                }else {
                    self.heightHeaderTableView = 0
                }
                
                self.mTableView.reloadData()
                
            }
            else {
                FQUtilities.showAlert(self, title: FQString.Alert.error, message: (error?.localizedDescription)!)
            }
        }
    }
    
    func getVenueDetail(_ venue:FQVenue){
        FQUtilities.showLoading(FQString.loading)
        venue.getDetail { (json, error) in
            FQUtilities.hideLoading()
            if(error == nil)
            {
                let venueSelected = FQVenue(json!["response"]["venue"], recommend: false)
                self.performSegue(withIdentifier: "SegueMainPushToMap", sender: venueSelected)
            }else{
                FQUtilities.showAlert(self, title: FQString.Alert.error, message: FQString.Alert.getCurrentVeueError)
            }
        }
    }
    
    func checkEditText(){
        startTimer()
    }
    
    //MARK: - Timer
    func startTimer() {
        endTimer()
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        
    }
    
    @objc func updateTime() {
        let searchText = self.mSearchTextField.text!
        if(searchText.count >= 1){
            getVeneSuggest(searchText)
        }
        endTimer()
    }
    
    func endTimer() {
        countdownTimer.invalidate()
    }
    
    // MARK: - Location manager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("locationManager didUpdateLocations")
        FQUtilities.hideLoading()
        currentLocation = locations.last?.coordinate
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("locationManager didFailWithError")
        FQUtilities.showAlert(self, title: FQString.Alert.error, message: (error.localizedDescription))
        FQUtilities.hideLoading()
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        self.didSearch()
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
//        let searchText = textField.text!
//        print(searchText)
//        if(searchText.count >= 1){
//            getVeneSuggest(searchText)
//        }
        checkEditText()
    }

     // MARK: - Action
   
}

extension FQMainVC: UITableViewDelegate, UITableViewDataSource{
    // MARK: - TableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! SearchCell
        
        let venue = searchResults[indexPath.row];
        
        cell.setupView(venue)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.view.endEditing(true)
        let venue = searchResults[indexPath.row];
        getVenueDetail(venue)
        //self.performSegue(withIdentifier: "SegueMainPushToMap", sender: venue)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textAlignment = .center
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titleHeaderTableView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(heightHeaderTableView)
    }
}

