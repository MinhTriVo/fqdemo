//
//  FQAPIManager.swift
//  FQDemo
//
//  Created by TriVM on 1/18/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import Foundation

//MARK: - Venue
struct Venue{
    static let client_id = "EUCXHP4CDJBQ4HZJQN20M21BNGNE2KPZMD5ALZAYF1M4PJQF" // visit developer.foursqure.com for API key
    static let client_secret = "OUGCKLVS02VLFPC3JEEVHOFO4AEKZIBUX2PR5KAOMOM4SKQK" // visit developer.foursqure.com for API key
    static let version = "20140723" // version API
}

//MARK: - APIManager enum
class APIManager{
    static let search = "venues/search"
    static let recommend = "venues/explore"
    static let suggest = "venues/suggestcompletion"
    static let venues = "venues"
    static let tips = "tips"
    static let photos = "photos"
    static let categories = "venues/categories"
}

