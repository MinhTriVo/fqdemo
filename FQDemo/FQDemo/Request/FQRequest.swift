//
//  FQRequest.swift
//  FQDemo
//
//  Created by TriVM on 1/18/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON
import FoursquareAPIClient

let client = FoursquareAPIClient(clientId: Venue.client_id, clientSecret: Venue.client_secret, version: Venue.version)

class CommonService{
    
    static func venueSearch(_ currentLocation:CLLocationCoordinate2D, keySearch: String, numberItem: Int, completion: @escaping (_ data: JSON?, _ error: Error?) -> Void ) {
        
        if (!checkInternet()) {
            return;
        }
        let myLocation = "\(currentLocation.latitude),\(currentLocation.longitude)"
        var limit = numberItem
        
        if (numberItem < 0 || numberItem > 50) {
            limit = 50
        }
        let parameter: [String: String] = [
            "ll": myLocation,
            "limit": "\(limit)",
            "query": keySearch
        ];
        client.request(path: APIManager.search , parameter: parameter) { result in
            switch result {
            case let .success(data):
                let json = try! JSON(data: data)
                //print("\(json)")
                completion(json, nil)
            case let .failure(error):
                // Error handling
                completion(nil, error)
            }
        }
    }
    
    static func venueRecommend(_ currentLocation:CLLocationCoordinate2D, category:String, numberItem: Int, completion: @escaping (_ data: JSON?, _ error: Error?) -> Void ) {
        
        if (!checkInternet()) {
            return;
        }
        let myLocation = "\(currentLocation.latitude),\(currentLocation.longitude)"
        var limit = numberItem
        
        if (numberItem < 0 || numberItem > 50) {
            limit = 50
        }
        let parameter: [String: String] = [
            "ll": myLocation,
            "limit": "\(limit)",
            "query": category
        ];
        client.request(path: APIManager.recommend , parameter: parameter) { result in
            switch result {
            case let .success(data):
                let json = try! JSON(data: data)
                //print("\(json)")
                completion(json, nil)
            case let .failure(error):
                // Error handling
                completion(nil, error)
            }
        }
    }
    
    static func venueCategories(_ currentLocation:CLLocationCoordinate2D, completion: @escaping (_ data: JSON?, _ error: Error?) -> Void ) {
        
        if (!checkInternet()) {
            return;
        }
        let parameter = [String: String]()
        client.request(path: APIManager.categories , parameter: parameter) { result in
            switch result {
            case let .success(data):
                let json = try! JSON(data: data)
                //print("venueCategories\n \(json)")
                completion(json, nil)
            case let .failure(error):
                // Error handling
                completion(nil, error)
            }
        }
    }
    
    static func venueSuggest(_ currentLocation:CLLocationCoordinate2D, keySearch: String, numberItem: Int, completion: @escaping (_ data: JSON?, _ error: Error?) -> Void ) {
        
        if (!checkInternet()) {
            return;
        }
        let myLocation = "\(currentLocation.latitude),\(currentLocation.longitude)"
        var limit = numberItem
        
        if (numberItem < 0 || numberItem > 50) {
            limit = 50
        }
        let parameter: [String: String] = [
            "ll": myLocation,
            "limit": "\(limit)",
            "query": keySearch
        ];
        client.request(path: APIManager.suggest , parameter: parameter) { result in
            switch result {
            case let .success(data):
                let json = try! JSON(data: data)
                //print("\(json)")
                completion(json, nil)
            case let .failure(error):
                // Error handling
                completion(nil, error)
            }
        }
    }
    
    static func venueDetail(_ venue:FQVenue, numberItem: Int, completion: @escaping (_ data: JSON?, _ error: Error?) -> Void ) {
        
        if (!checkInternet()) {
            return;
        }
        var limit = numberItem
        if (numberItem < 0 || numberItem > 50) {
            limit = 50
        }
        let pathUrl = String(format:"%@/%@",APIManager.venues, venue.id!)
        let parameter: [String: String] = [
            "limit": "\(limit)"
        ];
        client.request(path: pathUrl , parameter: parameter) { result in
            switch result {
            case let .success(data):
                let json = try! JSON(data: data)
                //print(json)
                completion(json, nil)
            case let .failure(error):
                // Error handling
                completion(nil, error)
            }
        }
    }
    
    static func venueTips(_ venue:FQVenue, numberItem: Int, completion: @escaping (_ data: JSON?, _ error: Error?) -> Void ) {
        
        if (!checkInternet()) {
            return;
        }
        var limit = numberItem
        if (numberItem < 0 || numberItem > 50) {
            limit = 50
        }
        let pathUrl = String(format:"%@/%@/%@",APIManager.venues, venue.id!, APIManager.tips)
        let parameter: [String: String] = [
            "limit": "\(limit)",
            "sort": "recent"
        ];
        client.request(path: pathUrl , parameter: parameter) { result in
            switch result {
            case let .success(data):
                let json = try! JSON(data: data)
                //print(json)
                completion(json, nil)
            case let .failure(error):
                // Error handling
                completion(nil, error)
            }
        }
    }
    
    static func venuePhotos(_ venue:FQVenue, numberItem: Int, completion: @escaping (_ data: JSON?, _ error: Error?) -> Void ) {
        
        if (!checkInternet()) {
            return;
        }
        var limit = numberItem
        if (numberItem < 0 || numberItem > 50) {
            limit = 50
        }
        let pathUrl = String(format:"%@/%@/%@",APIManager.venues, venue.id!, APIManager.photos)
        let parameter: [String: String] = [
            "limit": "\(limit)",
        ];
        client.request(path: pathUrl , parameter: parameter) { result in
            switch result {
            case let .success(data):
                let json = try! JSON(data: data)
                //print(json)
                completion(json, nil)
            case let .failure(error):
                // Error handling
                completion(nil, error)
            }
        }
    }
    
    static func checkInternet() -> Bool{
        //print("checkInternet")
        if (!FQUtilities.checkNetworkStatus()) {
            FQUtilities.showAlert(FQUtilities.topViewController()!, title: FQString.Alert.error, message: FQString.Alert.messageNotInternet)
            FQUtilities.hideLoading()
            return false;
        }
        return true
    }
    
}


