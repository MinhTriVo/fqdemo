//
//  FQString.swift
//  FQDemo
//
//  Created by TriVM on 1/20/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import Foundation

struct FQString {
    struct Alert {
        static let waring = "Warning"
        static let error = "Error"
        static let messageEnanleLocation = "You must enable loaction serivce to use application"
        static let messageWarningEnanleLocation = "Do you want enable setting loaction serivce?"
        static let messageSearch = "Please enter a venue"
        static let messageErrorDetail = "Venue is not available"
        static let messageNoTips = "Venue is not tips"
        static let messageTipsFail = "Get list venue tips is fail"
        static let messagePhotosFail = "Get list photo is fail"
        static let dismissButton = "Dismiss"
        static let okButton = "OK"
        static let messageNotInternet = "Internet is not available"
        static let getCurrentLocationError = "Current location is not available"
        static let getCurrentVeueError = "Get current venue detail is fail"
    }
    
    static let no_data = "NO DATA"
    static let no_internet = "NO INTERNET"
    static let loading = "Loading..."
    static let getCurrentLocation = "Get current location..."
}
