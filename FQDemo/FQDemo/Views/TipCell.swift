//
//  TipCell.swift
//  FQDemo
//
//  Created by TriVM on 1/21/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import UIKit
import AlamofireImage

class TipCell: UITableViewCell {
    //MARK: - IBOUTLET
    @IBOutlet weak var mDescriptionTextView: UITextView!
    @IBOutlet weak var mUserNameLabel: UILabel!
    @IBOutlet weak var mImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupView(_ tip:FQTip) {
        self.mUserNameLabel.text = String(format:"%@ %@", tip.firstName!, tip.lastName!)
        self.mDescriptionTextView.text = tip.text!
        
        configImage(tip, placeholderImage: UIImage(named:"no-image-icon")!)
    }
    
    func configImage(_ tip:FQTip, placeholderImage: UIImage) {
        
        let sizeStr = String(format: "%dx%d", Int(mImageView.bounds.size.width), Int(mImageView.bounds.size.height))
        let urlImage = String(format: "%@%@%@",tip.prefix!, sizeStr ,tip.suffix!)
        
        if (urlImage == sizeStr) {
            mImageView.image = UIImage(named:"no-image-icon")
            return;
        }
        let sizeFrame = mImageView.frame.size
        mImageView.af_setImage(
            withURL: URL(string: urlImage)!,
            placeholderImage: placeholderImage,
            filter: AspectScaledToFillSizeWithRoundedCornersFilter(size: sizeFrame, radius: 0.0),
            imageTransition: .crossDissolve(0.2)
        )
    }
}
