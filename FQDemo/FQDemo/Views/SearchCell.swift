//
//  SearchCell.swift
//  mrJitters
//
//  Created by Ryan Kotzebue on 6/7/16.
//  Copyright © 2016 Ryan Kotzebue. All rights reserved.
//

import UIKit
import AlamofireImage

class SearchCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var mIconImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupView(_ venue:FQVenue) {
        self.title.text = venue.name
        self.distance.text = "\(venue.distance!)m"
        self.address.text = venue.address
        configImage(venue, placeholderImage: UIImage(named:"no-image-icon")!)
    }
    
    func configImage(_ venue:FQVenue, placeholderImage: UIImage) {
        
        let category = venue.category
        if(category == nil)
        {
            return
        }
        
        let sizeStr = String(format: "%d", Int(mIconImageView.bounds.size.width))
        let urlImage = String(format: "%@%@%@",category!.prefix!, sizeStr ,category!.suffix!)
        
        if (urlImage == sizeStr) {
            mIconImageView.image = UIImage(named:"no-image-icon")
            return;
        }
        let sizeFrame = mIconImageView.frame.size
        mIconImageView.af_setImage(
            withURL: URL(string: urlImage)!,
            placeholderImage: placeholderImage,
            filter: AspectScaledToFillSizeWithRoundedCornersFilter(size: sizeFrame, radius: 0.0),
            imageTransition: .crossDissolve(0.2)
        )
    }
}
