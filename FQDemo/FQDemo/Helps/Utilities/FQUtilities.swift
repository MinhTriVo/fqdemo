//
//  FQUtilities.swift
//  FQDemo
//
//  Created by TriVM on 1/18/18.
//  Copyright © 2018 TriVM. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import PKHUD
class FQUtilities{
    
    static func showAlert(_ viewcontroller: UIViewController, title: String, message: String) {
        //print("showAlert")
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: FQString.Alert.dismissButton, style: UIAlertActionStyle.default, handler: nil))
        viewcontroller.present(alert, animated: true, completion: nil)
    }
    
  
    static func showLoading(_ message:String){
        HUD.show(.progress)
    }
    static func hideLoading(){
        HUD.hide()
    }
    
    static func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    static func checkNetworkStatus() -> Bool{
        return NetworkReachabilityManager()!.isReachable
    }
}
